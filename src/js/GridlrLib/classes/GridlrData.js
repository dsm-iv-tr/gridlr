/**
    @alias GridlrData

    @description Represents a Gridlr data structure.

    @constructor
*/
GridlrLib.GridlrData = function () {

  if (!(this instanceof GridlrLib.GridlrData)) {
      return new GridlrLib.GridlrData();
  }

  // STATE
  this.internalData = {};
  this.lastAction = undefined;

  GridlrLib.togglePropertyWritable(this, 'internalData');
  GridlrLib.togglePropertyWritable(this, 'lastAction');

};

/**
    @alias GridlrData.prototype.addItem

    @description Adds an item to a data source.

    @param {Object} ev - the event triggering the function.
    @param {Item} objItemDef - a structure representing a single item in the grid.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrData.prototype.addItem = function (ev, objItemDef) {

  GridlrLib.togglePropertyWritable(this, 'internalData');

  if ((this.internalData[objItemDef.uniq_id] = objItemDef) === true) {

    GridlrLib.togglePropertyWritable(this, 'internalData');

    return true;

  } else {

    GridlrLib.togglePropertyWritable(this, 'internalData');

    return false;
  }

};

GridlrLib.GridlrData.prototype.moveItem = function (ev, objItemInfo) {

  if ((this.internalData[objItemInfo.uniq_id].box_num = objItemInfo.box_num) === true) {

    GridlrLib.togglePropertyWritable(this, 'internalData');

    return true;

  } else {

    GridlrLib.togglePropertyWritable(this, 'internalData');

    return false;
  }

};

/**
    @alias GridlrData.prototype.removeItem

    @description Removes an item from a data source.

    @param {Object} ev - the event triggering the function.
    @param {Item} objItemDef - a structure representing a single item in the grid.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrData.prototype.removeItem = function (ev, objItemDef) {

  GridlrLib.togglePropertyWritable(this, 'internalData');

  if ((delete this.internalData[objItemDef.uniq_id])) {

      GridlrLib.togglePropertyWritable(this, 'internalData');

      return true;

  } else {

      GridlrLib.togglePropertyWritable(this, 'internalData');

      return false;
  }

};

/**
    @alias GridlrData.prototype.clearItem

    @description Clears an item in a data source.

    @param {Object} ev - the event triggering the function.
    @param {Item} objItemDef - a structure representing a single item in the grid.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrData.prototype.clearItem = function (ev, objItemDef) {

  GridlrLib.togglePropertyWritable(this, 'internalData');

  if ((this.internalData[objItemDef.uniq_id] = null) === null) {

    GridlrLib.togglePropertyWritable(this, 'internalData');

    return true;

  } else {

    GridlrLib.togglePropertyWritable(this, 'internalData');

    return false;
  }

};

/**
    @alias GridlrData.prototype.setLastAction

    @description Set lastAction to track the state of what happened last.

    @param {Object} ev - the event triggering the function.
    @param {Object} objAction - information about the last action that happened.
*/
GridlrLib.GridlrData.prototype.setLastAction = function (ev, objAction) {

  GridlrLib.togglePropertyWritable(objAction.trueThis, 'lastAction');

  // retain old box number on move in 'old_box_num'
  if (objAction.wutHappened === "move") {
    objAction.theItem.old_box_num = this.internalData[objAction.theItem.uniq_id].box_num;
  }
  this.lastAction = objAction;

  GridlrLib.togglePropertyWritable(objAction.trueThis, 'lastAction');
};

/**
    @ignore

    @alias GridlrData.prototype.itemUndo

    @description Undo the last Item() add or remove.

    @param {Object} ev - the event triggering the function.
*/
GridlrLib.GridlrData.prototype.itemUndo = function (ev) {
  if (this.lastAction !== undefined) {

    var l = this.lastAction;
    var lastItem = l.theItem;
    var realItem = this.internalData[lastItem.uniq_id];

    if (l.wutHappened === 'add') {
      ($.publish("/gridlr/notify", {
        strLocID: '.js-notification-area',
        strID: 'error_' + $({}).uniqueId().attr('id'),
        strMessage: lastItem.title + " was mistakenly added, I'll remove it.",
        intTime: 1500
      }));

      $.publish("/item/remove", realItem);

    } else if (l.wutHappened === 'remove') {

      ($.publish("/gridlr/notify", {
        strLocID: '.js-notification-area',
        strID: 'error_' + $({}).uniqueId().attr('id'),
        strMessage: lastItem.title + " was mistakenly removed, I'll add it again.",
        intTime: 1500
      }));

      $.publish("/item/add", new GridlrLib.Item({
        uniq_id: lastItem.uniq_id,
        box_num: lastItem.box_num,
        title: lastItem.title
      }));

    } else if (l.wutHappened === 'move') {

      $.publish("/item/remove", realItem);

      $.publish("/item/add", new GridlrLib.Item({
        uniq_id: lastItem.uniq_id,
        box_num: lastItem.old_box_num,
        title: realItem.title
      }));
    }

  } else {
    ($.publish("/gridlr/notify", {
      strLocID: '.js-notification-area',
      strID: 'error_' + $({}).uniqueId().attr('id'),
      strMessage: "There's nothing to undo!",
      intTime: 1500
    }));
  }
};

/**
    @alias GridlrData.prototype.doJSON

    @description Returns JSON-formatted data suitable for storing.

    @param {Object} ev - the event triggering the function.

    @returns {Object} a JSON string of data upon success, false otherwise.
*/
GridlrLib.GridlrData.prototype.doJSON = function (ev) {

  var dataIn = JSON.stringify(this.internalData);

  if (dataIn !== undefined) {
    return dataIn;
  }

};

/**
    @alias GridlrData.prototype.importJSON

    @description Fills a GridlrData with data given in JSON format. Useful, maybe.

    @param {Object} ev - the event triggering the function.
    @param {Object} objGridlrJSON - JSON-formatted Gridlr data.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrData.prototype.importJSON = function (ev, objGridlrJSON) {

  var dataOut = JSON.parse(objGridlrJSON);

  if (dataOut !== undefined) {

    GridlrLib.togglePropertyWritable(this, 'internalData');

    this.internalData = dataOut;

    GridlrLib.togglePropertyWritable(this, 'internalData');

    return true;
  } else {
    return false;
  }

};

/**
    @alias GridlrData.prototype.resetiData

    @description Clears the internalData property entirely. You'll want to do this when you swap dates for the grid.

    @param {Object} ev - the event triggering the function.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrData.prototype.resetiData = function (ev) {

  GridlrLib.togglePropertyWritable(this, 'internalData');
  this.internalData = {};
  GridlrLib.togglePropertyWritable(this, 'internalData');

};
