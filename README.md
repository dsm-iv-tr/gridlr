# Gridlr

## Rationale

### Problem Definition
The Solve-It grid (conceptualized by Tamara Rosier, Ph.D.) is a useful tool for adults with ADHD, but has no computerized implementation. Adults with ADHD often find it difficult to track traditional paper charts and complex histories due to deficits in working memory and attention, and thus obtaining a history of "how well" time was spent is arduous at best and impossible at worst. The [original article](http://www.additudemag.com/adhd/article/print/11845.html) that inspired this application is mirrored in *OriginalArticle.md* in case the link goes dead.

### Approach and Justification
We approach this problem with an iterative, flexible development methodology. While the outcome is well-understood, the requirements of development will (and have) changed during the project development time. Long-term predictability is not very important, and there is likely to be little cost to downstream changes.

### Languages and Toolchain
In general, the toolchain and languages selected emphasize flexibility and modularity. Gulp, Jade, Bower and SASS are industry-specific, popular, and possess a wide base of support. jQuery and jQuery UI are easy to use and save time during development, and users are likely to have cached the libraries already, saving on load time.

### Library Design
As the toolchain suggests, our preference is for loosely-coupled units of functionality, so GridlrLib is an object-oriented JavaScript library designed to use a publish-subscribe pattern. The API will be usable without tapping into the event chain, and this allows it to be a little more flexible in the case that other developers want to implement a Gridlr of their own.

## Build

### Requirements
* Node >= 6.2.0
* Gulp >= 3.9.1
* Bower >= 1.7.9

### Setup

```
$ npm install
$ bower install
$ gulp [ARGS]... [TASK]...
```
#### Gulp: Tasks
- `default`: build, serve, and watch for changes.
- `build`: only build.

#### Gulp: Arguments
- `--production`: run in production environment.

### Use
```
http://localhost:3000/
```

### Documentation
```
http://localhost:3000/doc/
```

### Debugging

To obtain debug messages about events (and other interesting things) on the console, set DEBUGMODE before instantiating objects in the library:
```
GridlrLib.DEBUGMODE = true;
```
