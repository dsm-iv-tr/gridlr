# // TODO:

## Miro
* Linear data visualization (see beautiful art).

## Troy
* ~~Implement move `Item()` functionality.~~
* Implement some kind of fun reward system.
* ~~Implement undo.~~
* Store settings into localStorage key: `GridlrStorage_GlobalSettings`.

### Input
* Move item.
    * ~~Change `box_num` (on move)~~.
* ~~Undo.~~
    * ~~"one-step" the undo, because I don't think it's necessary to keep a history.~~

### Settings
* AJAX modals for History, Settings, Help
  * Daily = `index.html`, all other pages as ajax modals in 'drawer'.
  * `$().tabs()` - jQuery UI

### ADHD-specific concerns
* Distractibility: reduce this wherever we can, such as in design and colour selection
* Impulsivity: reduce this wherever we can, especially when operating on items
* Focus attention on elements only when necessary: for example, highlighting operable things to differentiate them based on state

### Special Needs
* Screen readers: add appropriate tags to grid items and controls.
* Colourblindness: this can be handled in theming.

### Attractive New ASCII Data Visualization
* Monochrome, reduces memory usage.
* ASCII, very portable across platforms.
* Easily copy and paste information from the visualization.

```
==========
==========
==========                    ==========		
==========    ==========      ==========		
==========    ==========      ==========      ==========
--------------------------------------------------------
Low energy    High energy     Low energy      High energy
        Low reward                    High reward
```
