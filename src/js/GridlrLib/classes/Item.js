/**
  @alias Item

  @description Represents a grid item, essentially a typedef.

  @constructor

  @property {string} date - the date the Item was added.
  @property {integer} uniq_id - a unique identifier. Pass "auto" unless you have a reason not to.
  @property {integer} box_num - the box the Item belongs in.
  @property {string} title - the title of the Item.

  @example
            var theItem = {
              box_num: 0,
              title: 'title'
            };

            var x = new GridlrLib.Item(theItem);
*/
GridlrLib.Item = function (objConfItem) {

  if (!(this instanceof GridlrLib.Item)) {
      return new GridlrLib.Item();
  }

  if (objConfItem.uniq_id === "auto") {
    objConfItem.uniq_id = $({}).uniqueId().attr('id');
  }

  GridlrLib.setObjectProperties(this, {
    uniq_id: objConfItem.uniq_id,
    box_num: objConfItem.box_num,
    title: objConfItem.title
  });

};
