/**
    @alias Gridlr

    @description Represents a Gridlr.

    @listens /item/add
    @listens /item/remove
    @listens /item/move

    @listens /gridlr/notify

    @listens /gridlr/save_data
    @listens /gridlr/load_data

    @listens /gridlr/debug

    @constructor
*/
GridlrLib.Gridlr = function () {

  // Enforce stricter constructor behaviour.
  if (!(this instanceof GridlrLib.Gridlr)) {
      return new GridlrLib.Gridlr();
  }

  // Keep original reference for pub/sub callbacks.
  var that = this;

  if (GridlrLib.DEBUGMODE === true) {
    var eventstreams = new Array(
        '/item/add',
        '/item/remove',
        '/item/move',

        '/gridlr/notify',
        '/gridlr/save_data',
        '/gridlr/load_data',
        '/gridlr/reset_idata',
        '/gridlr/set_date',
        '/gridlr/set_last',
        '/gridlr/undo',
        '/gridlr/debug'
    );

    eventstreams.forEach(function (cv, index, arr) {
      $.subscribe(cv, function (ev, data) {
        console.log("EVENT:", ev.type);
        console.log("EVENT DATA:", data);
      });
    });

  }

  // Data and View objects. Feel free to dump these if you need to. They love it!
  // STATE
  this.objData = new GridlrLib.GridlrData();
  this.objView = new GridlrLib.GridlrView();
  this.objDate = undefined;

  // TODO: During code cleanup, turn $.subscribe()'s into the callback form.
  $.subscribe("/item/add", function (ev, evArgs) {
    $.publish("/gridlr/set_last", {
      'theItem': evArgs,
      'wutHappened': 'add',
      'trueThis': that.objData
    });
  });
  $.subscribe("/item/add", this.objData.addItem.bind(this.objData));
  $.subscribe("/item/add", this.objView.addToGrid.bind(this.objView));
  $.subscribe("/item/add", this.objView.refreshAutocomplete.bind(this.objView, this.objData.internalData));
  $.subscribe("/item/add", function (ev, evArgs) {
    $.publish("/gridlr/save_data", that.objDate);
  });

  $.subscribe("/item/remove", function (ev, evArgs) {

    $.publish("/gridlr/set_last", {
      'theItem': evArgs,
      'wutHappened': 'remove',
      'trueThis': that.objData
    });
  });
  $.subscribe("/item/remove", this.objData.removeItem.bind(this.objData));
  $.subscribe("/item/remove", this.objView.removeFromGrid.bind(this.objView));
  $.subscribe("/item/remove", this.objView.refreshAutocomplete.bind(this.objView, this.objData.internalData));
  $.subscribe("/item/remove", function (ev, evArgs) {
    $.publish("/gridlr/save_data", that.objDate);
  });

  $.subscribe("/item/move", function (ev, evArgs) {

      $.publish("/gridlr/set_last", {
        'theItem': evArgs,
        'wutHappened': 'move',
        'trueThis': that.objData
      });

  });

  $.subscribe("/item/move", this.objData.moveItem.bind(this.objData));
  $.subscribe("/item/move", function (ev, evArgs) {
    $.publish("/gridlr/save_data", that.objDate);
  });

  $.subscribe("/gridlr/set_last", this.objData.setLastAction.bind(this.objData));
  $.subscribe("/gridlr/undo", this.objData.itemUndo.bind(this.objData));

  $.subscribe("/gridlr/init", this.initGridlr.bind(this));
  $.subscribe("/gridlr/destroy", this.destroyGridlr.bind(this));

  $.subscribe("/gridlr/set_date", this.setGridlrDate.bind(this));
  $.subscribe("/gridlr/set_date", this.objView.setGridlrViewDate.bind(this.objView));

  $.subscribe("/gridlr/reset_idata", this.objData.resetiData.bind(this.objData));
  $.subscribe("/gridlr/save_data", this.saveData.bind(this));
  $.subscribe("/gridlr/load_data", this.loadData.bind(this));

  $.subscribe("/gridlr/notify", this.objView.flashMsg.bind(this.objView));

  GridlrLib.togglePropertyWritable(this, 'objData');
  GridlrLib.togglePropertyWritable(this, 'objView');
  GridlrLib.togglePropertyWritable(this, 'objDate');

  $.publish("/gridlr/set_date", new Date(Date.now()));

};

/**
    @alias Gridlr.prototype.initGridlr

    @description Initialize Gridlr with a specific element.

    @param {Object} ev - the event triggering the function.
    @param {string} strPageElem - the page element to bind to, with leading '#'.

    @returns {boolean} true upon success, throws an Error() otherwise.
*/
GridlrLib.Gridlr.prototype.initGridlr = function (ev, strPageElem) {

  if ($(strPageElem).length > 0) {

    this.objView.createView(strPageElem, this.objDate);

    return true;

  } else {
    throw new Error('Gridlr: ' + strPageElem + ' not found.');
  }

};

/**
    @alias Gridlr.prototype.destroyGridlr

    @description Destroy Gridlr! Grrrrar!

    @param {Object} ev - the event triggering the function.
    @param {string} strPageElem - the page element that contains Gridlr, with leading '#'.

    @returns {boolean} true upon success, throws an Error() otherwise.
*/
GridlrLib.Gridlr.prototype.destroyGridlr = function (ev, strPageElem) {

  if ($(strPageElem).length > 0) {

    $(strPageElem).remove();

    return true;

  } else {
    throw new Error('Gridlr: ' + strPageElem + ' not found.');
  }

};

/**
    @alias Gridlr.prototype.bindDataSource

    @description Binds a Gridlr() to a specific data source object. If you call this without a valid parameter, the current Gridlr() is given a new data object. Be careful.

    @param [GridlrData()] objDataSource - the new data source to bind to.

    @returns {boolean} representing success, or false if a new data object was assigned.
*/
GridlrLib.Gridlr.prototype.bindDataSource = function (objDataSource) {

  if (objDataSource !== null) {

    GridlrLib.togglePropertyWritable(this, 'objData');

    this.objData = objDataSource;

    GridlrLib.togglePropertyWritable(this, 'objData');

    return true;

  } else {
      /*
        ~ ♩ ♪ ♫ don't let me / don't let me /
        don't let me down / down down down / wub wub wubbbb ♩ ♪ ♫ ~

        https://www.youtube.com/watch?v=qMH0Xglh7GA
      */

      GridlrLib.togglePropertyWritable(this, 'objData');

      this.objData = new GridlrLib.GridlrData();

      GridlrLib.togglePropertyWritable(this, 'objData');

      return false;
  }

};

/**
    @alias Gridlr.prototype.releaseDataSource

    @description Releases a GridlrData() from a Gridlr(). Be careful.
*/
GridlrLib.Gridlr.prototype.releaseDataSource = function () {

  GridlrLib.togglePropertyWritable(this, 'objData');

  this.objData = undefined;

  GridlrLib.togglePropertyWritable(this, 'objData');

};

/**
    @alias Gridlr.prototype.saveData

    @param {Object} ev - the event triggering the function.
    @param {Date} objSaveDate - a Date representing the storage location to save to.

    @description Save data to LocalStorage.
*/
GridlrLib.Gridlr.prototype.saveData = function (ev, objSaveDate) {

  localStorage.setItem("GridlrStorage_" + GridlrLib.getTheDateString(objSaveDate), this.objData.doJSON());

};

/**
    @alias Gridlr.prototype.loadData

    @param {Object} ev - the event triggering the function.
    @param {Date} objDate - the Date to load data from.

    @description Load data from LocalStorage and fill grid with Items.
*/
GridlrLib.Gridlr.prototype.loadData = function (ev, objDate) {

  var xjs = JSON.parse(localStorage.getItem("GridlrStorage_" + GridlrLib.getTheDateString(objDate)));

  if (xjs !== null) {
    if (Object.keys(xjs).length > 0) {
      for (var x in xjs) {
        $.publish("/item/add", new GridlrLib.Item({
          uniq_id: xjs[x].uniq_id,
          box_num: xjs[x].box_num,
          title: xjs[x].title
        }));
      }
    }
  } else {
    $.publish("/gridlr/debug", "Nothing to load!");
  }

  // Refresh storage once.
  $.publish("/gridlr/save_data", objDate);

};

/**
    @alias Gridlr.prototype.setGridlrDate

    @description Set the objDate property.

    @param {Object} ev - the event triggering the function.
    @param {Date} objNewDate - the Date object to set to.

    @returns {boolean} representing success or failure.
*/
GridlrLib.Gridlr.prototype.setGridlrDate = function (ev, objNewDate) {

  GridlrLib.togglePropertyWritable(this, 'objDate');

  this.objDate = objNewDate;

  GridlrLib.togglePropertyWritable(this, 'objDate');

};
