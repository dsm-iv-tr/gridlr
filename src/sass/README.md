# Sass Documentation

## Where stuff goes:
- `_base.scss`: All type selectors (e.g. `body`, `h1`).
- `_fonts.scss`: All `@font-face` rules.
- `_layout.scss`: All declarations affecting the layout structure of the page (e.g. `width: 100px`, `display: flex`).
- `_theme.scss`: All declarations affecting cosmetic concerns (e.g. `background: #fff`, `cursor:pointer`).
- `_variables.scss`: Commonly used values (for DRY-ness). Also sets the modular scale.
- `style.scss`: Concatenates everything together (except themes).
- `themes/*.scss`: One file for each theme. More information on theming below.

## Naming convention:
Gridlr uses BEM, exactly as [described in this article](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/).

## Modular-scale:
Gridlr uses the [modular-scale Sass plugin](https://github.com/modularscale/modularscale-sass) for sizing many elements. These measurements look like this: `ms(0)`. The resulting pixel values are exponentially related.

## Themes:
Each file in the `themes/` directory represents one theme and outputs its own CSS stylesheet. It consists of three main parts in the following order:
1. Variables.
2. `@import '../_theme'`.
3. Additional rules (optional).

This copies the rules from `_theme.scss`, using the variables in the `theme/*.scss` file.
