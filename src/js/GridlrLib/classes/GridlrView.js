/**
    @alias GridlrView

    @description Represents a Gridlr's view layer.

    @constructor
*/
GridlrLib.GridlrView = function () {

  if (!(this instanceof GridlrLib.GridlrView)) {
      return new GridlrLib.GridlrView();
  }

  // STATE
  this.autocompleteData = undefined;
  this.viewDate = undefined;

  GridlrLib.togglePropertyWritable(this, 'viewDate');
  GridlrLib.togglePropertyWritable(this, 'autocompleteData');

};

/**
    @alias GridlrView.prototype.createView

    @description Creates a working view layer.

    @fires /gridlr/notify

    @param {string} strPageElem - the element to work with.
    @param {Date} objViewDate - the Date to initialize the view to.
*/
GridlrLib.GridlrView.prototype.createView = function (strPageElem, objViewDate) {

    // Cache 'this', DOM, and the day the view was initialized on.
    var that = this;

    var original_day = objViewDate;

    // Cache DOM elements
    this.$gridElement = $(strPageElem);
    this.$quadrant = this.$gridElement.find('.js-quadrant');
    this.$dateControls = this.$gridElement.find('.js-date-controls');
    this.$dateline = this.$dateControls.find('.js-dateline');
    this.$undoControl = this.$gridElement.find('.js-undo-control');

    // Bind undo button
    this.$undoControl.on("click", ".js-undo", function (event) {
      $.publish("/gridlr/undo");
    });

    // Bind keyboard-shortcut undo / redo
    this.$gridElement.keydown(function(event) {

      // Prevent undo / redo when editing items
      // (so user doesn't trigger Gridlr undo if they only want browser undo).
      if ($(document.activeElement).is('input, [contenteditable="true"]')) {
          return;

      } else if (event.which === 90 && event.ctrlKey) {
        event.preventDefault();

        // Redo: `ctrl-shift-z`
        if (event.shiftKey) {
          $.publish('/gridlr/redo');

        // Undo: `ctrl-z`
        } else {
          $.publish('/gridlr/undo');
        }
      }
    });

    // Set initial date
    this.$dateline.text(GridlrLib.getTheDateString(original_day));

    // Bind date selection
    this.$dateControls.on("click", ".js-select-date", function (event) {
      var button_target_date = $(this).data('target-date');
      var target_date;

      if (button_target_date === 'previous') {
        target_date = new Date(that.viewDate.valueOf() - 1000 * 60 * 60 * 24);

      } else if (button_target_date === 'original') {
        target_date = original_day;

      } else if (button_target_date === 'next') {
        target_date = new Date(that.viewDate.valueOf() + 1000 * 60 * 60 * 24);

        if (target_date.valueOf() > Date.now().valueOf()) {
          target_date = original_day;
          ($.publish("/gridlr/notify", {
            strLocID: '.js-notification-area',
            strID: 'error_' + $({}).uniqueId().attr('id'),
            strMessage: "Gridlr isn't a time machine, but check to see if there's a node package that meets your needs.",
            intTime: 2500
          }));
        }
      }

      that.removeAllItems();

      that.$dateline.text(GridlrLib.getTheDateString(target_date));

      $.publish("/gridlr/reset_idata");
      $.publish("/gridlr/set_date", target_date);
      $.publish("/gridlr/load_data", target_date);
    });

    this.$quadrant.each(function() {
      var $thisQuadrant = $(this);
      var intQuadrant = $thisQuadrant.data('intQuadrant');
      var $thisItemInput = $thisQuadrant.find('.js-add-item');
      var $thisItemList = $thisQuadrant.find('.js-item-list');

      // Set up autocompleteness.
      $thisItemInput.autocomplete({
        delay: 500,
        minLength: 1,
        position: {
          my: 'left bottom',
          at: 'left top'
        },

        select: function () {
          $(this).val('');
        },

        source: function (req, resp) {
          /*
              TODO: Optimize.

              This will probably get slow with large lists.
          */
          if (that.autocompleteData !== undefined) {
            resp(that.autocompleteData);
          } else {
            // La, la, la, singin' 'bout nothin'!
            resp();
          }
        }
      }).on("click", function (event) {
        $(this).autocomplete("search");
      });

      // Bind input to add item.
      $thisItemInput.on("keydown", function (event) {
        if (event.which === 13) {   // keycode 13 = RETURN / ENTER

          // Cache input value.
          var thisVal = $thisItemInput.val();

          if (thisVal.trim()) {
            event.preventDefault();

            $.publish("/item/add", new GridlrLib.Item({
              uniq_id: 'auto',
              box_num: intQuadrant,
              title: thisVal
            }));

            $thisItemInput.val('');
            $thisItemInput.autocomplete("close");

          } else {
            event.preventDefault();

            ($.publish("/gridlr/notify", {
              strLocID: '.js-notification-area',
              strID: 'error_' + $({}).uniqueId().attr('id'),
              strMessage: "Gridlr doesn't like your blank item. Write a more good one.",
              intTime: 1500
            }));

          }
        }
      });

      // Make item list sortable.
      $thisItemList.sortable({
        connectWith: '.js-item-list',
    		cancel: '*:focus',
    		cursor: 'move',
        handle: '.js-sortable-handle',
        receive: function(event, ui) {
          $.publish('/item/move', {
            uniq_id: ui.item.attr('id'),
            box_num: intQuadrant
          });
        }
      });
    });

    $.publish("/gridlr/load_data", objViewDate);
};

/**
    @alias GridlrView.prototype.addToGrid

    @description Adds an item to the view.

    @param {Object} ev - the event triggering the function.
    @param {Item} objItemDef - a structure representing a single item in the grid.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrView.prototype.addToGrid = function (ev, objItemDef) {

  var $itemList = this.$gridElement.find('.js-item-list[data-int-quadrant="' + objItemDef.box_num + '"]');

  var $item = $('<li>')
      .attr({
        'id': objItemDef.uniq_id,
        'class': 'gridlr-item js-sortable-item'
      });

  var $label = $('<span>')
      .attr({
        'class': 'gridlr-item__label js-sortable-handle',
        'contenteditable': true
      })
      .on({
        dblclick: function() {
          $(this).focus();
        },
        blur: function() {
          if (!this.innerText.trim()) {
            $(this).text(objItemDef.title);

            ($.publish("/gridlr/notify", {
              strLocID: '.js-notification-area',
              strID: 'error_' + $({}).uniqueId().attr('id'),
              strMessage: "Gridlr doesn't like your blank item. Write a more good one.",
              intTime: 1500
            }));
          }
        },
        keydown: function(event) {
          if (event.which === 13) {   // keycode 13 = `return` / `enter`
            event.preventDefault();
            $(this).blur();
          }
        }
      })
      .text(objItemDef.title);


  var $remove = $('<span>')
      .attr({
        'class': 'gridlr-item__button button'
      })
      .click(function(){
        $.publish('/item/remove', {
          uniq_id: objItemDef.uniq_id,
          box_num: objItemDef.box_num,
          title: objItemDef.title
        });
      })
      .text('×');

  $item.append($label);
  $item.append($remove);

  if (($item.appendTo($itemList)).length > 0) {
    return true;
  } else {
    return false;
  }
};

/**
    @alias GridlrView.prototype.removeFromGrid

    @description Removes an item from the view.

    @fires /gridlr/notify

    @param {Object} ev - the event triggering the function.
    @param {Item} objItemDef - a structure representing a single item in the grid.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrView.prototype.removeFromGrid = function (ev, objItemDef) {

  var itemToRemove = this.$gridElement.find('#' + objItemDef.uniq_id);

  if (itemToRemove.length > 0) {

    itemToRemove.remove();

		($.publish("/gridlr/notify", {
			strMessage: 'Removed "' + objItemDef.title + '" from your day.',
			intTime: 1500
		}));

    return true;

  } else {
    ($.publish("/gridlr/notify", {
          strLocID: '.js-notification-area',
          strID: 'error_' + $({}).uniqueId().attr('id'),
          strMessage: "Whoooaa, that entry so scandalous, I swear that Gridlr can't handle it.",
          intTime: 500
    }));
    return false;
  }

};

/**
    @alias GridlrView.prototype.removeAllItems

    @description Removes all items from the view, but NOT from the GridlrData() or LocalStorage.

    @fires /gridlr/notify

    @param {Object} ev - the event triggering the function.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrView.prototype.removeAllItems = function (ev) {
  var itemsToRemove = this.$gridElement.find('.gridlr-item');

  $(itemsToRemove).each(function (idx, elem) {
    elem.remove();
  });

};

/**
    @alias GridlrView.prototype.setGridlrViewDate

    @description Sets the viewDate property. You'll need to do this when you swap dates.

    @param {Object} ev - the event triggering the function.
    @param {Date} objViewDate - the Date to set to.

    @returns {boolean} representing success or failure.
*/
GridlrLib.GridlrView.prototype.setGridlrViewDate = function (ev, objViewDate) {

  GridlrLib.togglePropertyWritable(this, 'viewDate');

  this.viewDate = objViewDate;

  GridlrLib.togglePropertyWritable(this, 'viewDate');

};

/**
    @alias GridlrView.prototype.refreshAutocomplete

    @description Refresh the list of autocomplete entries.

    @param {Object} objInternalData
*/
GridlrLib.GridlrView.prototype.refreshAutocomplete = function (objInternalData) {
  var x = [];

  for (var t in objInternalData) {
    x.push(objInternalData[t].title);
  }

  this.autocompleteData = x;
};

/**
    @alias GridlrView.prototype.flashMsg

    @description Shows a message in the notification area.

    @param {Object} ev - the event triggering the function.
    @param {Item} objBubbleConf - a structure representing a message to display.
*/
GridlrLib.GridlrView.prototype.flashMsg = function (ev, objBubbleConf) {

  if (objBubbleConf.strLocID === undefined) {
      objBubbleConf.strLocID = '.js-notification-area';
  }

    // Queue multiple notifications.
    $(objBubbleConf.strLocID).queue(function () {

      var that = this;

      var tempMsg = $('<div>')
                        .attr({
                          'id': objBubbleConf.strID,
                          'class': 'notification-area__notification'
                        })
                        .text(objBubbleConf.strMessage);

      $(objBubbleConf.strLocID).append(tempMsg);

      setTimeout(function () {
        $(that).dequeue();
        $(tempMsg).remove();
      }, (objBubbleConf.intTime));

    });
};
