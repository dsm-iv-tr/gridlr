# How Latent Emotions Tangle Up Your Time Management — and a Way Forward

## Tired of wasting time? This new ADHD-friendly tool — called the Solve-it Grid — will help you get more done every day without feeling drained or guilty.

### by Tamara Rosier, Ph.D.

Adults with ADHD have a peculiar relationship with time, often investing it frivolously or ineffectively. Many of us feel anxiety, guilt, or self-loathing when we think about how we use our days, resulting in more wasted energy and time. Routine tasks, like laundry and paying the bills, frustrate us. When we look at those who don’t have ADHD, we wonder, “Why are ordinary tasks so difficult for us to accomplish?”

The reason is that we conceptualize time and tasks differently. If we understand how we view our to-dos, we can re-frame our work and play to create a new, healthier life.

#### How We See Time

We see our world in predictable ways. We divide our tasks into two categories — fun and not fun. The ADHD brain searches the horizon for the interesting, shiny, curious, remarkable, and exciting, and we call it all “fun.” We are captivated by fun: It affects our levels of drive, motivation, and happiness.

People with ADHD see the world through their emotions. Our brains are wired to detect even mild levels of stimulation. It doesn’t matter if it is positive or negative stimulation; something gets our attention and we respond. We experience negative emotional stimulation when the electric bill we avoided paying is overdue. We experience positive emotional stimulation when we look forward to going golfing with friends. Some tasks, though, have no emotional stimulation. Emptying the dishwasher or doing homework doesn’t excite us, and therefore is not interesting to us.

It’s not that the ADHD brain actively seeks enjoyment, but it is responding to a learned (or even an unlearned) emotional cue. We create emotional cues through positive or negative feelings about previous experiences — feeling guilty when we forget to load the dishwasher or can’t finish our math homework. Accepting this about ourselves, and incorporating it into our understanding of time and tasks, will help us get things done without frustration.

#### The Solve-it Grid: Your Ultimate Time Tool

When we consider how our brain engages the world — through fun and emotional stimulation — we can determine how we spend our time and energy. Many of my clients use the Solve-it Grid, but you can make your own on a piece of paper. I developed this grid after working with clients who wanted to analyze how their motivation and energy affect each other.

The Red quadrant includes tasks and activities that aren’t fun but are emotionally stimulating. When you tackle an activity in this quadrant, you work yourself into a heightened emotional state to complete the task. Procrastination, delaying a task until it requires immediate attention, is a way of dealing with a task in the Red quadrant. We get anxious and feel like we need to respond quickly. Doing activities in the Red quadrant is addictive, because we connect our frantic feelings with achievement. This makes us more likely to rely on frenzied actions in order to be productive. We get things done by pushing them off until the last minute, but we are drained afterward.

My former client, Mark, was addicted to tasks that weren’t fun, but were emotionally stimulating. He said that doing these tasks, like waiting to start a report until an hour before it was due, not filling his gas tank until it was nearly empty, or, as he would say, putting out fires at his job, made him feel “effective and alive.” He overloaded his calendar, procrastinated, and pushed himself to deliver. Contrary to what he believes, Mark can’t thrive living mostly in the Red quadrant. He will burn out quickly.

Our first response to tasks in the Yellow quadrant is “yuck.” We find them distasteful and boring. They are the things that we do to be like a grownup — housework, logging sales calls at the end of the week, and balancing our checkbooks. Such tasks aren’t difficult; they just don’t hold our interest. Because most of my clients see items in this quadrant as a nuisance, a chore, or an irritation, they neglect, avoid, or wish the items away.

The Blue quadrant is a seductive trap. When we want to distract ourselves from tasks in the Yellow and Red quadrants, we stop here. We tell ourselves that we will check Facebook for “just a moment,” and two hours of clicking go by. Just like the other quadrants, Blue quadrant favorites are different for each person — games on our phone or binge-watching a TV show — but the danger is the same: wasting time by avoiding what we need to do.

Spending some time in this quadrant isn’t bad, but the time we spend prevents us from being productive. Some time spent in Blue is useful because it gives our busy brains a rest and allows for quiet play. The “Goldilocks rule” applies here: Too little of Blue makes us anxious for play and rest, too much Blue makes us sluggish and resistant to accomplishing tasks. Look for the “just right” amount of time in Blue.

The Green quadrant is every ADHD person’s favorite. The activities here are fun and emotionally stimulating. The Green quadrant gives us space to build relationships, reflect on personal growth, be creative, and enjoy our lives. It often refocuses our purpose and goals, reminding us of what is important. Green can have a transformative effect on our lives. Activities in this quadrant include spending time with family and friends, going for a walk on a beautiful day, visiting an art museum, or watching the sunset.

We feel refreshed, more hopeful, and closer to our authentic selves in the Green quadrant. We love to spend time there, but many of us don’t. We feel as if we don’t deserve it or don’t want to take the time to invest in it. In a recent workshop, one participant sighed, “I punish myself all the time by not allowing Green time into my day.” We often settle for Blue because we think it doesn’t drain our energy as much as Green activities.

Once my clients use the Solve-it Grid, they place their tasks in quadrants and think about them in a new way. One client began her session this way: “OK, I hate this week. The activities all fall in the Yellow. I hate it. But if I don’t do this stuff, I know that it will all turn Red.” When I nodded, she went on: “I do know that I have some Green tasks this week — I will see my granddaughter tomorrow. If I can get the Yellow tasks done, I will be able to relax with her.”

#### Analyzing Our Patterns

ADHDers can use the Solve-it Grid to evaluate how we use our time and energy. When we analyze our patterns, we can change how we think and act. Many of my clients have predictable patterns that they prefer to use. For instance, Kevin is a go-getter who knows how to get things done — in the Red quadrant. Kevin is exhilarated by and addicted to “hitting life hard.” After a few coaching sessions, he realized that, although he was energized by these activities, he couldn’t sustain it. The grid helped him see that his pattern was Red for a week, followed by a depressed, nonproductive Blue for two weeks. He alternated between high anxiety and depression, not because he was bipolar but because he didn’t know how to use and balance his time and energy.

Many diagnosed with ADHD lead lives of frustration, guilt, and fatigue. For instance, a person might:

> Avoid the mundane task in the Yellow quadrant until it turns into a bright Red emergency.
> Avoid uninteresting tasks in Yellow and find distractions in Blue, spending hours there.
> Have so much fun in Green that she doesn’t want to engage activities in other quadrants.
> Deny herself Green time because she believes she hasn’t earned it. Instead, she does things in the Red and Yellow quadrants because, as one of my clients said, “that is what being a grownup looks like to me.”
A Case Study in Getting Things Done

Martyn used the grid to increase his productivity at work. Because he worked at home, he had little structure in his day. He was paid in commissions, had flexible hours, but didn’t like many of his work activities. While he was motivated to make more money, and could do the extra work, he floundered.

Martyn used the grid in four phases:
1. Learning how to use the Solve-it Grid;
2. Analyzing how he currently uses his time and energy;
3. Detecting his work patterns;
4. Creating a plan to address the patterns.

After learning about the grid, he noticed that doing well in his job meant performing many small tedious tasks in the Yellow quadrant. Instead of engaging the Yellow quadrant, he defaulted to Blue quadrant activities and felt guilty about it. After avoiding Yellow for a while, he fired himself up to turn those tiresome tasks into Red quadrant activities. Exhausted, he would return to Blue activities.

Martyn decided that his use of the Blue quadrant created many of his work challenges. His plan included dividing his day into small and manageable Yellow work zones, made tolerable by a few fun activities in the Blue quadrant. He learned to treat himself to Green by scheduling time to think about ways to develop a side business. The grid helped him balance his time and his life.

> Copyright © 1998 - 2016 New Hope Media LLC. All rights reserved.
