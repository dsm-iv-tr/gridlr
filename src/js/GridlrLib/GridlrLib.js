/**
    @module GridlrLib

    @description GridlrLib is the API library for Gridlr.

    @copyright Troy Rennie and Miro Wagner, 2016.

    @example
                // Debug messages on console.
                GridlrLib.DEBUGMODE = true;

                var thegridlr = new GridlrLib.Gridlr();

                thegridlr.initGridlr('#js-page-element');

                // ...
                // ...

                // Optionally, you may later destroy Gridlr.
                thegridlr.destroyGridlr('#js-page-element');
*/
var GridlrLib = GridlrLib || {};

GridlrLib = {
  DEBUGMODE: false
};

/**
    @alias GridlrLib.togglePropWritable

    @description  Toggle the 'writable' attribute on a property.

    @param {Object} obj - the object to use.
    @param {Object} prop - the property to toggle protection on.
*/
GridlrLib.togglePropertyWritable = function (obj, prop) {
  /*
      ~ ♩ ♪ ♫ New year's resolution - to write something of value /
      New year's resolution - to write something would be fine ♩ ♪ ♫ ~

      https://www.youtube.com/watch?v=RxK-Qey3QdU
  */
  if (Object.getOwnPropertyDescriptor(obj, prop).writable === true) {
    Object.defineProperty(obj, prop, {
      writable: false
    });

    $.publish("/gridlr/debug", prop + " made not writable.");

  } else {
    Object.defineProperty(obj, prop, {
      writable: true
    });
    $.publish("/gridlr/debug", prop + " made writable.");
  }
};

/**
    @alias GridlrLib.setObjectProperties

    @description  Set multiple properties on an object.

    @param {Object} objObject - the object to set properties on.
    @param {Object} objProps - a set of properties in standard 'key: value' format.
*/
GridlrLib.setObjectProperties = function (objObject, objProps) {
  for (var singleProp in objProps) {
    objObject[singleProp] = objProps[singleProp];
  }
};

/**
    @alias GridlrLib.getTheDateString

    @description Get the date in the format we use if given a "new Date()"-recognized argument. The default is to return the current full date in DD-MM-YYYY format.

    @param {Object} objDate - an argument to the Date() constructor.

    @returns {string} - a string representing the date.
*/
GridlrLib.getTheDateString = function (objDate) {

  var x;

  if (objDate instanceof Date) {
    x = objDate;
  } else {
    x = new Date(Date.now());
  }

  return x.getFullYear() + "-" + (x.getMonth() + 1) + "-" + x.getDate();

};

/**
    @alias GridlrLib.showDialog

    @description  Show a dialog.

    @param {string} name - the name of the dialog to show.
*/
GridlrLib.showDialog = function (name) {
	$('.js-dialog').hide(null);
	$('.js-' + name).show(null);
};

/**
    @alias GridlrLib.generateChartObject

    @param {Date} objDataDate - the Date to get data from.

    @description  Returns an object to configure Chart.js.

    @returns {Object} - an object containing the data for the Date given.
*/
GridlrLib.generateChartObject = function (objDataDate) {

  // TODO: Optimize. This is a rough draft.

  var dataPts = JSON.parse(localStorage.getItem("GridlrStorage_" + GridlrLib.getTheDateString(objDataDate)));

  var tempObj = {
    'box0': 0,
    'box1': 0,
    'box2': 0,
    'box3': 0
  };

  for (var y in dataPts) {
     tempObj['box' + dataPts[y].box_num] += 1;
  }

  var retObj = {
      type: 'bar',
      data: {
          labels: ["One", "Two", "Three", "Four"],
          datasets: [{
              label: '# of Items',
              data: [tempObj.box0, tempObj.box1, tempObj.box2, tempObj.box3],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  };

  return retObj;

};
