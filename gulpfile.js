/*
  Contents
  ========

  Require Node modules
  Variables
  Helpers
  Define tasks
*/



/*
  Require Node modules
  ====================

  Manual: Require `gulp` and other Node modules (but not gulp plugins).
*/
var gulp = require('gulp');
var bs = require('browser-sync');
var mainBowerFiles = require('main-bower-files');
var args = require('yargs').argv;

/*
  Automatic: require all gulp plugins found in package.json.

  The plugin becomes a property of the `plugins` object.
  Its name is camelized and drops the `gulp-` prefix,
  e.g. `gulp-some-plugin` becomes `plugins.somePlugin`
*/
var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins();

/*
  Variables
  =========
*/
var basePaths = {
  src: './src/',
  dest: './build/',
  bower: './bower_components/'
};

var paths = {
  bower: {
    root:  basePaths.bower,
    main:  mainBowerFiles()
  },
  src: {
    root:  basePaths.src,
    js:    basePaths.src + 'js/',
    sass:  basePaths.src + 'sass/',
    jade:  basePaths.src + 'jade/',
    img:   basePaths.src + 'img/',
    font:  basePaths.src + 'font/'
  },
  dest: {
    root:  basePaths.dest,
    img:   basePaths.dest + 'assets/img/',
    font:  basePaths.dest + 'assets/font/',
    css:   basePaths.dest + 'assets/css/',
    js:    basePaths.dest + 'assets/js/',
    jsdoc: basePaths.dest + 'doc/'
  }
};


/*
  Helpers
  =======
*/
var handleErrors = function() {
  return plugins.plumber({
    errorHandler: plugins.notify.onError("<%= error.message %>")
  });
};



/*
  Define tasks
  ============

  https://github.com/gulpjs/gulp/blob/master/docs/API.md#gulptaskname--deps--fn
*/


/*
  Bundle Bower dependencies:
*/
gulp.task('vendor-js', function() {
  var filterJS = plugins.filter('**/*.js');
  var filename = 'vendor.min.js';

  return gulp.src(mainBowerFiles({includeDev: args.production ? false : true}))
    .pipe(handleErrors())
    .pipe(filterJS)
    .pipe(plugins.newer(paths.dest.js + filename))
    .pipe(plugins.if(args.production, plugins.uglify()))
    .pipe(plugins.concat(filename))
    .pipe(gulp.dest(paths.dest.js));
});


/*
  Compile Jade templates:
*/
gulp.task('jade', function() {
  return gulp.src(paths.src.jade + '*.jade')
    .pipe(handleErrors())
    .pipe(plugins.newer(paths.dest.root + '**/*.html'))
    .pipe(plugins.jade({
      locals: {
        args: args
      },
      pretty: args.production ? false : true
    }))
    .pipe(gulp.dest(paths.dest.root));
});


/*
  Images:
  - Lossless compression of png/jpg/gif/svg.
*/
gulp.task('img', function() {
  var match = '/*';

  return gulp.src(paths.src.img + match)
    .pipe(handleErrors())
    .pipe(plugins.newer(paths.dest.img + match))
    .pipe(plugins.imagemin())
    .pipe(gulp.dest(paths.dest.img));
});

/*
  Fonts:
  - Copy fonts to build dir.
*/
gulp.task('font', function() {
  var match = '/*';

  return gulp.src(paths.src.font + match)
    .pipe(handleErrors())
    .pipe(plugins.newer(paths.dest.font + match))
    .pipe(gulp.dest(paths.dest.font));
});


/*
  Javascript:
  - Minify.
  - Concatenate into one file (scripts.js).
  - Write sourcemaps.
*/
gulp.task('js', function() {
  var filename = 'scripts.js';

  return gulp.src([
      paths.src.js + 'GridlrLib/*.js',
      paths.src.js + 'GridlrLib/classes/*.js',
      paths.src.js + '*.js'
    ])
    .pipe(handleErrors())
    .pipe(plugins.newer(paths.dest.js + filename))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.if(args.production, plugins.uglify()))
    .pipe(plugins.concat(filename))
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(paths.dest.js));
});


/*
  Generate documentation for JS:
*/
gulp.task('js-doc', function() {

  if (!args.production) {
    return gulp.src([paths.src.js + '**/*.js'], {read: false})
      .pipe(handleErrors())
      .pipe(plugins.newer(paths.dest.jsdoc))
      .pipe(plugins.jsdoc3({
        opts: {
          destination: paths.dest.jsdoc,
          readme: 'README.md'
        }
      }));
  } else {
    return true;
  }

});


/*
  Stylesheets:
  - Compile CSS from SCSS.
  - Add vendor prefixes.
  - Write sourcemaps.
  - Inject changes into browser.
*/
gulp.task('style', function() {
  return gulp.src(paths.src.sass + '**/*.scss')
    .pipe(handleErrors())
    .pipe(plugins.newer(paths.dest.css + '**/*.css'))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({
      outputStyle: args.production ? 'compressed' : 'expanded'
    }))
    .on('error', plugins.sass.logError)
    .pipe(plugins.autoprefixer({browsers: 'last 2 versions'}))
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(paths.dest.css))
    .pipe(bs.stream({match: '**/*.css'}));
});


/*
  Build only (don't serve or watch).
*/
gulp.task('build', ['vendor-js', 'jade', 'img', 'js', 'js-doc', 'style', 'font']);


/*
  Default task (run with `gulp` from CLI).

  Build, then serve and watch for changes.
*/
gulp.task('default', ['build'], function() {

  // Start server
  bs.init({
    // see https://www.browsersync.io/docs/options
    server: paths.dest.root,
    notify: false,
    open: false
  });

  /*
    Watch for changes:

    In order to reload the browser only after the relevant tasks have completed, it's necessary to create an additional task that will use `bs.reload` as a callback, and then watch that task.
  */

  // vendor-js
  gulp.task('reload:vendor-js', ['vendor-js'], bs.reload);
  gulp.watch([paths.bower.root + '**/*'], ['reload:vendor-js']);

  // jade
  gulp.task('reload:jade', ['jade'], bs.reload);
  gulp.watch([paths.src.jade + '**/*'], ['reload:jade']);

  // img
  gulp.task('reload:img', ['img'], bs.reload);
  gulp.watch([paths.src.img + '**/*'], ['reload:img']);

  // js, js-doc
  gulp.task('reload:js', ['js', 'js-doc'], bs.reload);
  gulp.watch([paths.src.js + '**/*'], ['reload:js']);

  // style (injects changes without reloading)
  gulp.watch([paths.src.sass + '**/*'], ['style']);

});
